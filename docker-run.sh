#!/bin/sh
#
# Docker image
#
DOCKER_IMAGE=ubuntu_botan:1.0
NETWORK=server_side
IP_ADDR=172.19.0.10

docker run --rm -it -v $(pwd):/usr/tls -w /usr/tls --name $1  --cap-add=NET_ADMIN --network $NETWORK --ip $IP_ADDR $DOCKER_IMAGE