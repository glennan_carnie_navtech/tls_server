#include "Credentials_manager.h"

namespace Security {

    std::vector<Botan::Certificate_Store*> 
    Credentials_manager::trusted_certificate_authorities(
        const std::string& type     [[maybe_unused]], 
        const std::string& hostname [[maybe_unused]]
    )
    {
        using std::vector;

        vector<Botan::Certificate_Store*> certificate_stores { };

        if (type == "tls_server") {
            return certificate_stores;
        }
        else {
            for (const auto& ptr : cert_stores) {
                certificate_stores.push_back(ptr.get());
            }
            return certificate_stores;
        }
    }


} // namespace Security