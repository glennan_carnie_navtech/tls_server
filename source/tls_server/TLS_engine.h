#ifndef TLS_ENGINE_H
#define TLS_ENGINE_H

#include <memory>
#include <atomic>

#include "botan/tls_session.h"
#include "botan/auto_rng.h"
#include "botan/tls_callbacks.h"
#include "botan/tls_alert.h"
#include "botan/tls_channel.h"

#include <botan/tls_policy.h>
#include <botan/x509path.h>
#include <botan/ocsp.h>
#include <botan/hex.h>


#include "Message_port.h"
#include "TLS_services.h"


namespace Botan {
    class Credentials_Manager;

    namespace TLS { 
        class Strict_Policy; 
        class Session_Manager;
    }
}

namespace Networking {

    class Session;
    class Botan_services;
    
    
    class TLS_engine {
    public:
        TLS_engine(
            TLS_services<TLS_policy::botan>& tls_srv,
            Session&        owner
        );

        void open();
        void close();

        // Ports
        //
        Message_port encrypt_in  { };
        Message_port encrypt_out { };
        Message_port decrypt_in  { };
        Message_port decrypt_out { };

    protected:
        friend class Encrypter;
        friend class Decrypter;

        void encryption_error();
        void decryption_error();

    private:
        // The Callback object is used by the Botan component 
        // (Server or Client) to route data into/out-of the 
        // Botan object.
        //
        class Callbacks : public Botan::TLS::Callbacks {
        public:
            Callbacks(TLS_engine& owner);

            void tls_record_received(uint64_t /*rec_no*/, const uint8_t buf[], size_t buf_len)  override;
            void tls_emit_data(const uint8_t buf[], size_t buf_len)                             override;
            bool tls_session_established(const Botan::TLS::Session& session)                    override;
            void tls_alert(Botan::TLS::Alert alert)                                             override;
            void tls_session_activated()                                                        override;
            
            // void tls_verify_cert_chain(
            //     const std::vector<Botan::X509_Certificate>& cert_chain,
            //     const std::vector<std::shared_ptr<const Botan::OCSP::Response>>& ocsp_responses,
            //     const std::vector<Botan::Certificate_Store*>& trusted_roots,
            //     Botan::Usage_Type usage,
            //     const std::string& hostname,
            //     const Botan::TLS::Policy& policy)                                               override;
        
        private:
            TLS_engine* tls_engine;
        };

        friend class Callbacks;
        Callbacks callbacks;
        
        // TLS::Channel is the base class for the Botan
        // client AND server
        //
        std::unique_ptr<Botan::TLS::Channel> channel { nullptr };

        Session* session;
    };

} // namespace Networking


#endif // TLS_ENGINE_H