#ifndef COMMS_SERVER_H
#define COMMS_SERVER_H

#include <thread>

#include <asio.hpp>

#include "TCP_server.h"
#include "Session_manager.h"
#include "Colossus_message.h"


namespace Networking {

    class Config;

    class Comms_server {
    public:
        Comms_server(Config& cfg);

        void start();
        void stop();

        Session::Pointer get_session(Session::ID id);

    private:
        // Execution context management
        //
        asio::io_context io_context { };
        std::thread t { };
        void run();

        // Internal components
        //
        Session_manager session_mgr;
        TCP_server      tcp_server;

    };

} // namespace Networking


#endif // COMMS_SERVER_H