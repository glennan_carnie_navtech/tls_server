
#include <algorithm>
#include <iterator>

#include "botan/tls_session_manager.h"
#include "botan/credentials_manager.h"
#include "botan/tls_policy.h"
#include "botan/tls_server.h"
#include "botan/tls_client.h"

#include "TLS_engine.h"
#include "TLS_services.h"
#include "Session.h"
#include "trace.h"


namespace Networking {

    // ------------------------------------------------------------------------------------------
    // TLS_engine
    //
    TLS_engine::TLS_engine(
        TLS_services<TLS_policy::botan>& tls_srv,
        Session&            owner
    ) :
        callbacks       { *this },
        session         { &owner }
    {
        using std::make_unique;

        channel = make_unique<Botan::TLS::Server>(
            callbacks,
            tls_srv.session_mgr(),
            tls_srv.credentials_mgr(),
            tls_srv.policy(),
            tls_srv.rng()
        );

        // NOTE:  
        // The TLS component is only fully open
        // once the TLS handshake has completed.
        encrypt_in.on_receive(
            [this](Message& msg)
            {
                try {
                    channel->send(msg.data(), msg.size());
                }
                catch (const std::exception& e) {
                    TRACE_MSG(e.what());
                    encryption_error();
                } 
            }
        );

        decrypt_in.on_receive(
            [this](Message& msg)
            {
                try {
                    channel->received_data(msg.data(), msg.size());
                }
                catch (const std::exception& e) {
                    TRACE_MSG(e.what());
                    encryption_error();
                }
            }
        );
    }


    void TLS_engine::open()
    {
        
    }
    
    
    void TLS_engine::close()
    {
        session->inactive(Session::Component::tls_engine);
    }



    void TLS_engine::encryption_error()
    {
        TRACE_MSG("Encryption error");
        session->report_internal_failure(Session::Component::tls_engine);
    }
    
    
    void TLS_engine::decryption_error()
    {
        TRACE_MSG("Decryption error");
        session->report_internal_failure(Session::Component::tls_engine);
    }


    // ------------------------------------------------------------------------------------------
    // TLS callbacks
    //
     TLS_engine::Callbacks::Callbacks(TLS_engine& owner) :
        tls_engine { &owner }
    {
    }

    void TLS_engine::Callbacks::tls_record_received(uint64_t rec_no [[maybe_unused]], const uint8_t buf[], size_t buf_len)
    {
        using std::copy_n;
        using std::move;
        using std::back_inserter;

        Message msg { };
        msg.reserve(buf_len);
        copy_n(buf, buf_len, back_inserter(msg));
        tls_engine->decrypt_out.post(move(msg));
    }


    void TLS_engine::Callbacks::tls_emit_data(const uint8_t buf[], size_t buf_len)
    {
        using std::copy_n;
        using std::move;
        using std::back_inserter;
        
        Message msg { };
        msg.reserve(buf_len);
        copy_n(buf, buf_len, back_inserter(msg));
        tls_engine->encrypt_out.post(move(msg));
    }


    bool TLS_engine::Callbacks::tls_session_established(const Botan::TLS::Session& session [[maybe_unused]])
    {
        return true;
    }


    void TLS_engine::Callbacks::tls_alert(Botan::TLS::Alert alert [[maybe_unused]])
    {
        TRACE_MSG("TLS alert signalled");
        TRACE_VALUE(alert.type_string());
        
        if (alert.is_fatal()) {
            assert(false);
        }
    }


    void TLS_engine::Callbacks::tls_session_activated()
    {
        // TLS is only active once the TLS handshake
        // is fully complete.
        //
        tls_engine->session->active(Session::Component::tls_engine);
    }


} // namespace Networking