#include <algorithm>
#include <cassert>

#include <iostream>

#include "Translator.h"
#include "Colossus_message.h"
#include "Session.h"


namespace Networking {

    // ------------------------------------------------------------------------------------------
    // Parser
    //
    Parser::Parser(Translator& owner) :
        translator { &owner }
    {
    }


    void Parser::open()
    {
        initialize_message();
    }


    void Parser::close()
    {
        // Clear out any potential messages
        // from the input stream
        //
        parse_input_stream();
    }


    void Parser::process(const Message& msg)
    {
        input_stream.push(msg);
        parse_input_stream();
    }


    void Parser::process(Message&& msg)
    {
        using std::move;

        input_stream.push(move(msg));
        parse_input_stream();
    }


    // This function attempts to construct a complete
    // and valid message from the input stream.  The
    // algorithm logic is as follows:
    // 
    // - If there's not enough data in the input stream to construct
    //   a complete header, wait until more data is pushed onto the
    //   input stream.
    //
    // - Read a header's worth of data and see if it is a valid
    //   message header.  If not, discard this data and try again.
    //   Repeat until we have a valid header, or run out of data on the
    //   input stream.
    //
    // - Read a payload's worth of data from the input stream.
    //   If there's not (yet) enough data on the input stream to read
    //   a full payload, retrieve what is available, then wait for the
    //   next insertion on the input stream.  Repeat until a full
    //   payload has been retrieved.
    //
    // - Output the message.
    //
    // - Repeat while there's still data on the input stream
    //
    // This algorithm is implemented as a simple state machine.  The 'do-'
    // functions represent the in-state behaviour; the 'on-' functions
    // represent the transition behaviours.
    //
    void Parser::parse_input_stream()
    {
        while (!input_stream.empty()) {
            current_state = (this->*state_table[current_state])();
        }
    }


    // Parser state machine implementation
    //
    std::array<Parser::State_behaviour, Parser::State::num_states> Parser::state_table {
        &Parser::do_read_header,
        &Parser::do_read_payload
    };


    // - Read a header's worth of data and see if it is a valid
    //   message header.  If there's not enough data in the input 
    //   stream to construct a complete header, wait until more data 
    //   is pushed onto the input stream.
    //   Repeat until we have a valid header.
    //   If the message has a payload configure the parser to read
    //   it; otherwise forward on the (completed) message.
    //
    Parser::State Parser::do_read_header()
    {
        auto num_bytes = input_stream.pop_n_into(write_pos, (Colossus::TCP_message::header_size() - current_header_size));
        current_header_size += num_bytes;
        write_pos           += num_bytes;

        auto msg_ptr = Colossus::TCP_message::overlay_onto(message.data());

        if (current_header_size < Colossus::TCP_message::header_size()) {
            return reading_header;
        }

        if (!msg_ptr->is_valid()) {
            TRACE_MSG("Invalid header");
            
            initialize_message();
            return reading_header;
        }

        if (msg_ptr->payload_size() > 0) {
            on_header_received();
            return reading_payload;
        }
        else {
            on_message_complete();
            return reading_header;
        }  
    }


    // Set up the current message prior to receiving
    // its payload.
    //
    void Parser::on_header_received()
    {
        // Set up for reading the payload
        //
        auto msg_ptr = Colossus::TCP_message::overlay_onto(message.data());

        auto header_sz  { Colossus::TCP_message::header_size() };
        auto payload_sz { msg_ptr->payload_size() };

        message.resize(header_sz + payload_sz);

        // Resize will have invalidated any iterators,
        // so we have to reset msg_ptr
        //
        msg_ptr = Colossus::TCP_message::overlay_onto(message.data());
        write_pos = msg_ptr->payload_begin();
        current_payload_size = 0;
    }


    // - Read a payload's worth of data from the input stream.
    //   If there's not (yet) enough data on the input stream to read
    //   a full payload, retrieve what is available, then wait for the
    //   next insertion on the input stream.  Repeat until a full
    //   payload has been retrieved.
    //
    Parser::State Parser::do_read_payload()
    {
        using std::move;

        auto msg_ptr = Colossus::TCP_message::overlay_onto(message.data());

        auto num_bytes = input_stream.pop_into(write_pos, msg_ptr->payload_end());
        current_payload_size += num_bytes;
        write_pos            += num_bytes;

        if (current_payload_size != msg_ptr->payload_size()) {
            return reading_payload;
        }
        
        on_message_complete();
        return reading_header;
    }


    void Parser::on_message_complete()
    {
        translator->parse_out.post(move(message));
        initialize_message();
    }


    void Parser::initialize_message()
    {
        message.clear();
        message.resize(Colossus::TCP_message::header_size());

        auto msg_ptr = Colossus::TCP_message::overlay_onto(message.data());
        write_pos            = msg_ptr->begin();
        current_header_size  = 0;
        current_payload_size = 0;
    }


    // ------------------------------------------------------------------------------------------
    // Marshaller
    //
    Marshaller::Marshaller(Translator& owner) :
        translator { &owner }
    {
    }


    void Marshaller::open()
    {
    }


    void Marshaller::close()
    {
    }

    std::uint8_t Marshaller::next_message_num()
    {
        static std::uint8_t seq_num { 0 };

        return seq_num++;
    }


    void Marshaller::send(Colossus::TCP_message::Type type, std::string data)
    {
        using std::array;
        using std::uint8_t;
        using std::copy;
        using std::move;
        using std::begin;
        using std::end;

        // Construct an empty (zero-filled) buffer big
        // enough to hold the header and payload.
        //
        Message msg { };
        msg.resize(Colossus::TCP_message::header_size() + data.size());

        // Interpret the msg buffer as if it were a 
        // Colossus::TCP_message, for data copying
        //
        auto msg_ptr = Colossus::TCP_message::overlay_onto(msg.data());

        // Copy in the payload
        //
        copy(begin(data), end(data), msg_ptr->payload_begin());

        // Set the header details
        //
        msg_ptr->type(type);
        auto packet_sz = data.size();
        msg_ptr->payload_size(packet_sz);

        static constexpr array<uint8_t, 16> signature { 
            0x00, 0x01, 0x03, 0x03, 0x09, 0x09, 0x0F, 0x0F, 
            0x1F, 0x1F, 0x6F, 0x6F, 0x8F, 0x8F, 0xFE, 0xFE 
        };

        copy(begin(signature), end(signature), msg_ptr->signature_begin());

        // Transfer ownership to the next
        // processor in the pipeline.
        //
        translator->marshall_out.post(move(msg));
    }


    // ------------------------------------------------------------------------------------------
    // Translator
    //
    Translator::Translator(Session& owner) :
        parser     { *this },
        marshaller { *this },
        session    { &owner }
    {
        parse_in.on_receive(
            [this](Message& msg) 
            {
                parser.process(std::move(msg)); 
            }
        );

        marshall_in.on_receive(
            [this](Message_pair& msg_pair) 
            { 
                marshaller.send(msg_pair.first, std::move(msg_pair.second));
            }
        );

        parse_out.on_receive(
            [this](Message& msg)
            {
                session->message_received(move(msg));
            }
        );
    }


    void Translator::open()
    {
        parser.open();
        marshaller.open();
        session->active(Session::Component::translator);
    }


    void Translator::close()
    {
        session->inactive(Session::Component::translator);
        marshaller.close();
        parser.close();
    }

} // namespace Networking