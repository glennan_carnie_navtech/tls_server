#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <cstdint>
#include <functional>
#include <atomic>
#include <string>

#include "Message.h"
#include "Circular_buffer.h"
#include "Colossus_message.h"

#include "Message_port.h"

namespace Networking {

    class Translator;
    class Session;

    // ------------------------------------------------------------------------------------------
    //
    class Parser {
    public:
        Parser(Translator& owner);

        void open();
        void close();

        void process(const Message& msg);
        void process(Message&& msg);

    private:
        // Message parsing
        //
        void parse_input_stream();
    
        using Buffer = Utility::Circular_buffer<std::uint8_t, 8192>;

        Buffer      input_stream         {   };
        Message     message              {   };
        std::size_t current_header_size  { 0 };
        std::size_t current_payload_size { 0 };
        Colossus::TCP_message::Iterator write_pos { };

        // State machine for controlling parsing
        //
        enum State { reading_header, reading_payload, num_states };
        State current_state { reading_header };

        using State_behaviour = State(Parser::*)();
        State do_read_header();
        State do_read_payload();
        void  on_header_received();
        void  on_message_complete();
        void  initialize_message();

        static std::array<State_behaviour, State::num_states> state_table;

        Translator*  translator;
    };


    // ------------------------------------------------------------------------------------------
    //
    class Marshaller {
    public:
        Marshaller(Translator& owner);

        void send(Colossus::TCP_message::Type type, std::string data);
        void open();
        void close();

    private:
        std::uint8_t next_message_num();

        Translator* translator;
    };


    // ------------------------------------------------------------------------------------------
    //
    class Translator {
    public:
        Translator(Session& owner);

        void open();
        void close();
       
        Message_port parse_in     { };
        Message_port parse_out    { };
        Message_port marshall_out { };
        
        // Input to the Translator requires a unique port.
        //
        using Message_pair      = std::pair<Colossus::TCP_message::Type, std::string>;
        using Message_pair_port = Utility::Port<Message_pair>;
        
        Message_pair_port marshall_in  { };
       
    private:
        Parser     parser;
        Marshaller marshaller;

        Session* session;
    };

} // namespace Networking


#endif // TRANSLATOR_H