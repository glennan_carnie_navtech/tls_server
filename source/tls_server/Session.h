#ifndef SESSION_H
#define SESSION_H

#include <memory>

#include "asio.hpp"
#include "Connection.h"
#include "TLS_engine.h"
#include "Translator.h"
#include "Threadsafe_queue.h"
#include "Event_flags.h"

namespace Networking {

    class Session_manager;
    class Botan_services;
    
    class Session {
    public:
        using ID         = int;
        using Owning_ptr = std::shared_ptr<Session>;
        using Pointer    = Session*;

        static Owning_ptr create(
            asio::io_context&       context,
            asio::ip::tcp::socket   s,
            TLS_services<TLS_policy::botan>& tls_services,
            Session_manager&        parent, 
            ID identity
        );

        void open();
        void close();
        bool is_open() const;
        void send_message(Colossus::TCP_message::Type type, std::string data);
        Message get_message();
    
    protected:
        // Grant composite elements access to the protected interface
        //
        enum class Component { connection, tls_engine, translator };

        friend class Connection;
        friend class TLS_engine;
        friend class Translator;
        friend class Parser;
        
        Session(
            asio::io_context&       context,
            asio::ip::tcp::socket   s,
            TLS_services<TLS_policy::botan>& tls_services,
            Session_manager&        parent, 
            ID identity
        );
        
        void report_internal_failure(Component component);
        void do_send();
        
        template <typename T> void message_received(T&& msg);
        void active(Component component);
        void inactive(Component component);

    private:
        asio::io_context* io_context;

        Connection connection;
        TLS_engine tls_engine;
        Translator translator;

        Session_manager* session_mgr;
        ID id;

        using Message_pair        = std::pair<Colossus::TCP_message::Type, std::string>;
        using Outgoing_queue_type = Utility::Threadsafe_queue<Message_pair>;
        using Incoming_queue_type = Utility::Threadsafe_queue<Message>;

        Outgoing_queue_type send_queue    { };
        Incoming_queue_type receive_queue { };
        
        using Flags = Utility::Event_flags<8>;
        mutable Flags active_flags { };
        Flags::Bitmask all_component_flags() const;
    };


    template <typename T>
    void Session::message_received(T&& msg)
    {
        receive_queue.push(std::forward<T>(msg));
    } 
}


#endif // SESSION_H