#include <cstring>
#include <array>
#include <arpa/inet.h>
// #include "winsock.h" // For Windows

#include "Colossus_message.h"

namespace Networking {

    namespace Colossus {

        bool TCP_message::is_valid() const
        {
            return (
                is_signature_valid() &&                               // Signature is fixed
                static_cast<unsigned>(type()) <= 128 &&               // has a valid message type
                payload_size() < 1'000'000                            // Way bigger than any real payload
            );
        }


        bool TCP_message::is_signature_valid() const
        {
            using std::array;
            using std::begin;
            using std::uint8_t;
            using std::memcmp;

            static constexpr array<uint8_t, 16> valid_signature { 
                0x00, 0x01, 0x03, 0x03, 0x09, 0x09, 0x0F, 0x0F, 
                0x1F, 0x1F, 0x6F, 0x6F, 0x8F, 0x8F, 0xFE, 0xFE 
            };

            return (
                memcmp(
                    valid_signature.begin(), 
                    signature_begin(),
                    valid_signature.size()
                ) == 0
            ); 
        }

        std::size_t TCP_message::size() const
        {
            return (end() - begin());
        }


        std::uint32_t TCP_message::payload_size() const
        {
            return ntohl(header.payload_size);
        }


        void TCP_message::payload_size(std::uint32_t sz)
        {
            header.payload_size = htonl(sz);
        }


        TCP_message::Type TCP_message::type() const
        {
            return header.id;
        }


        void TCP_message::type(TCP_message::Type t)
        {
            header.id = t;
        }


        TCP_message::Iterator TCP_message::begin()
        {
            return reinterpret_cast<Iterator>(this);
        }


        TCP_message::Iterator TCP_message::end()
        {
            return payload_end();
        }


        TCP_message::Iterator TCP_message::payload_begin()
        {
            return reinterpret_cast<Iterator>(reinterpret_cast<Iterator>(&header) + sizeof(header));
        }


        TCP_message::Iterator TCP_message::payload_end()
        {
            return Iterator { payload_begin() + payload_size() };
        }


        TCP_message::Const_iterator TCP_message::begin() const
        {
            return reinterpret_cast<Const_iterator>(this);
        }


        TCP_message::Const_iterator TCP_message::end() const 
        {
            return payload_end();
        }


        TCP_message::Iterator TCP_message::signature_begin()
        {
            return &header.signature[0];
        }


        TCP_message::Const_iterator TCP_message::signature_begin() const
        {
            return &header.signature[0];
        }


        TCP_message::Iterator TCP_message::signature_end()
        {
            return &header.signature[16]; // One-past-the-end
        }


        TCP_message::Const_iterator TCP_message::signature_end() const
        {
            return &header.signature[16];
        }


        TCP_message::Const_iterator TCP_message::payload_begin() const
        {
            return reinterpret_cast<Const_iterator>(reinterpret_cast<Const_iterator>(&header) + sizeof(header));
        }


        TCP_message::Const_iterator TCP_message::payload_end() const
        {
            return Const_iterator { payload_begin() + header.payload_size };
        }

    }   // namespace Colossus

} // namespace Networking