#include <sstream>
#include "Error.h"

namespace Navtech::TLS {

    // ------------------------------------------------------------------
    //
    Error::Source::Source(Type t) :
        value { t }
    {
    }


    std::string Error::Source::to_string() const
    {
        static constexpr const char* strings[] { 
            "unknown", 
            "rx", 
            "tx", 
            "tls", 
            "translation"
        };

        return std::string { strings[value] };
    }


    bool Error::Source::operator==(const Error::Source& rhs) const
    {
        return (this->value == rhs.value);
    }


    bool Error::Source::operator!=(const Error::Source& rhs) const
    {
        return !operator==(rhs);
    }


    // ------------------------------------------------------------------
    //
    Error::Severity::Severity(Level l) :
        value { l }
    {
    }


    std::string Error::Severity::to_string() const
    {
        static constexpr const char* strings[] { 
            "info", 
            "warning", 
            "fatal"
        };

        return std::string { strings[value] };
    }


    bool Error::Severity::operator==(const Error::Severity& rhs) const
    {
        return (this->value == rhs.value);
    }


    bool Error::Severity::operator!=(const Error::Severity& rhs) const
    {
        return !operator==(rhs);
    }


    // ------------------------------------------------------------------
    //
    Error::Error(Error::Source source, Error::Severity severity) :
        src { source },
        sev { severity }
    {
    }


    Error::Error(Error::Source source, Error::Severity severity, const std::string& error_str) :
        src { source },
        sev { severity },
        str { error_str }
    {
    }


    const Error::Source& Error::source() const
    {
        return src;
    }


    const Error::Severity& Error::severity() const
    {
        return sev;
    }


    std::string Error::what() const
    {
        std::ostringstream oss { };
        oss << "[ Source: "   << src.to_string() << " ]";
        oss << "[ Severity: " << sev.to_string() << " ]";
        
        if (!str.empty()) oss << " - " << str;

        return oss.str();
    }

    
    std::ostream& operator<<(std::ostream& os, const Navtech::TLS::Error& e)
    {
        os << e.what();
        return os;
    }

} // namespace Navtech::TLS

