#ifndef ERROR_H
#define ERROR_H

#include <string>
#include <iostream>


namespace Navtech::TLS {

    class Error {
    public:
        class Source {
        public:
            enum Type { unknown, rx, tx, tls, translation };

            Source(Type t);
            std::string to_string() const;

            bool operator==(const Source& rhs) const;
            bool operator!=(const Source& rhs) const;
        
        private:
            Type value;
        };


        class Severity {
        public:
            enum Level { info, warning, fatal };

            Severity(Level l);
            std::string to_string() const;

            bool operator==(const Severity& rhs) const;
            bool operator!=(const Severity& rhs) const;
        
        private:
            Level value;
        };


        Error() = default;
        Error(Source source, Severity level);
        Error(Source source, Severity level, const std::string& error_str);

        const Source&    source() const;
        const Severity&  severity() const;
        std::string      what() const;

        friend std::ostream& operator<<(std::ostream& os, const Error& e);

    private:
        Source      src { Source::unknown };
        Severity    sev { Severity::info };
        std::string str { };
    };

} // namespace Navtech::TLS

#endif // ERROR_H