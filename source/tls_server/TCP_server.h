#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include <cstdint>

#include "asio.hpp"


namespace Networking {

    class Session_manager;
    class Server_config;

    class TCP_server {
    public:
        using Port = std::uint16_t;

        TCP_server(asio::io_context& cntxt, Server_config& cfg, Session_manager& sess_mgr);
        
        void start();
        void stop();

    private:
        void do_accept();
        void on_accepted(const asio::error_code& err, asio::ip::tcp::socket socket);

        asio::io_context* context;
        Session_manager*  session_mgr;
        
        asio::ip::tcp::acceptor acceptor;
    };

} // namespace Networking

#endif // TCP_SERVER_H