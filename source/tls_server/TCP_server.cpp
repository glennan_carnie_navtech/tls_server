#include <iostream>
#include <functional>

#include "TCP_server.h"
#include "Session_manager.h"
#include "Config.h"
#include "trace.h"

using namespace asio::ip;
using namespace std;

namespace Networking {

    TCP_server::TCP_server(asio::io_context& cntxt, Server_config& cfg, Session_manager& sess_mgr) :
        context     { &cntxt },
        session_mgr { &sess_mgr },
        acceptor    { *context, tcp::endpoint { tcp::v4(), cfg.listen_port } } 
    {
    }


    void TCP_server::start()
    {
        do_accept();
    }

    
    void TCP_server::stop()
    {
        
    }


    void TCP_server::do_accept()
    {
        acceptor.async_accept(bind(&TCP_server::on_accepted, this, placeholders::_1, placeholders::_2));
    }


    void TCP_server::on_accepted(const asio::error_code& err, tcp::socket socket)
    {
        if (!err) {
            session_mgr->create_session(move(socket));
        }
        else {
            TRACE_MSG(err.message());
        }

        do_accept();
    }

} // namespace Networking