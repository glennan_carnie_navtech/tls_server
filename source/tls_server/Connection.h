#ifndef CONNECTION_H
#define CONNECTION_H

#include <cstddef>
#include <cstdint>
#include <vector>
#include <chrono>

#include "asio.hpp"
#include "Message.h"
#include "FIFO.h"
#include "Message_port.h"
#include "Heap_array.h"
#include "Memory_types.h"
#include "Timeout.h"


using namespace Utility::Memory_literals;

namespace Networking {

    class Session;

    class Connection {
    public:
        Connection(asio::io_context& io, asio::ip::tcp::socket s, Session& parent);

        void open();
        void close();

        Message_port tx { };
        Message_port rx { };

    private:
        // --------------------------------------------------------------------
        //
        class Input_channel {
        public:
            Input_channel(asio::io_context& context, asio::ip::tcp::socket& s, Connection& parent);

            void open();
            void close();

        private:
            void do_read();
            void on_read_complete(const asio::error_code& err, std::size_t num_bytes);

            // Receiver constants
            //
            constexpr static std::size_t buffer_sz { 16_kB };
            constexpr static Utility::Timeout::Duration rx_timeout_duration { Utility::Timeout::forever };

            using Buffer = Utility::Heap_array<std::uint8_t, buffer_sz>;

            Buffer rx_buffer { };
            std::size_t bytes_recvd { };

            asio::io_context::strand rx_strand;
            asio::ip::tcp::socket* socket { nullptr };
            Connection* connection { nullptr };
            Utility::Timeout timeout;
        };

        // --------------------------------------------------------------------
        //
        class Output_channel {
        public:
            Output_channel(asio::io_context& context, asio::ip::tcp::socket& s, Connection& parent);

            void open();
            void close();

            void send(const Message& msg);
            void send(Message&& msg);

        private:          
            void do_write();
            void on_write_complete(const asio::error_code&, size_t num_bytes);

            using Buffer = std::vector<std::uint8_t>;

            Buffer pending_buffer   { };
            Buffer send_buffer      { };
            std::size_t bytes_sent  { };

            asio::io_context::strand tx_strand;
            asio::ip::tcp::socket* socket { nullptr };
            Connection* connection { nullptr };

            constexpr static Utility::Timeout::Duration tx_timeout_duration { std::chrono::seconds { 5 } };
            Utility::Timeout timeout;
        };

        // --------------------------------------------------------------------
        //
        void rx_error(const asio::error_code& err);
        void tx_error(const asio::error_code& err);

        asio::ip::tcp::socket socket;
        
        Input_channel  receiver;
        Output_channel sender;

        Session*  session { nullptr };
    };


} // namespace Networking


#endif // CONNECTION_H