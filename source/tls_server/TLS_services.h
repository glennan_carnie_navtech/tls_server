#ifndef TLS_SERVICES_H
#define TLS_SERVICES_H

#include <memory>

#include "Server_credentials_manager.h"
#include "Policy.h"
#include "Key_file.h"
#include "Certificate_file.h"
#include "trace.h"
#include "Config.h"
#include "TLS_policy.h"

#include "botan/system_rng.h"
#include "botan/tls_session_manager.h"


namespace Networking {

    class Config;
    
    // ----------------------------------------------------------------------------------------------------
    //
    template <TLS_policy>
    class TLS_services_traits { };


    template <>
    class TLS_services_traits<TLS_policy::botan> {
    public:
        using Credentials_manager       = Security::Server_credentials_manager;
        using Session_manager           = ::Botan::TLS::Session_Manager_In_Memory;
        using Policy                    = Security::TLS_policy;
        using Random_number_generator   = ::Botan::RandomNumberGenerator;

        static Random_number_generator& rng()
        {
            return ::Botan::system_rng();
        }
    };


    // ----------------------------------------------------------------------------------------------------
    //
    template <TLS_policy TLS_Ty>
    class TLS_services {
    public:
        using Credentials_manager       = typename TLS_services_traits<TLS_Ty>::Credentials_manager;
        using Session_manager           = typename TLS_services_traits<TLS_Ty>::Session_manager;
        using Policy                    = typename TLS_services_traits<TLS_Ty>::Policy;
        using Random_number_generator   = typename TLS_services_traits<TLS_Ty>::Random_number_generator;

        TLS_services() = default;
        TLS_services(const Config& cfg);

        inline Credentials_manager&     credentials_mgr() const;
        inline Session_manager&         session_mgr() const;
        inline Policy&                  policy() const;
        inline Random_number_generator& rng() const;

    private:
        std::unique_ptr<Credentials_manager> cred_mgr_ptr    { nullptr };
        std::unique_ptr<Session_manager>     session_mgr_ptr { nullptr };
        std::unique_ptr<Policy>              policy_ptr      { nullptr };
    };



    template <TLS_policy TLS_Ty>
    TLS_services<TLS_Ty>::TLS_services(const Config& cfg)
    {
        using std::make_unique;
        using Security::Key_file;
        using Security::Certificate_file;

        Key_file         key_file  { cfg.server_security.key };
        Certificate_file cert_file { cfg.server_security.certificate };

        if (!key_file)  key_file.create();
        if (!cert_file) cert_file.create(key_file);

        cred_mgr_ptr    = make_unique<Credentials_manager>();
        session_mgr_ptr = make_unique<Session_manager>(rng());
        policy_ptr      = make_unique<Policy>();
        assert(cred_mgr_ptr);
        assert(session_mgr_ptr);
        assert(policy_ptr);

        cred_mgr_ptr->set_certificate(cfg.server_security.certificate);
        cred_mgr_ptr->set_key(cfg.server_security.key, cfg.server_security.passphrase);

        if (!cred_mgr_ptr->load()) {
            TRACE_MSG("TLS credentials failed to load");
        }
    }


    template <TLS_policy TLS_Ty>
    typename TLS_services<TLS_Ty>::Credentials_manager& 
    TLS_services<TLS_Ty>::credentials_mgr() const
    {
        return *cred_mgr_ptr;
    }


    template <TLS_policy TLS_Ty>
    typename TLS_services<TLS_Ty>::Session_manager& 
    TLS_services<TLS_Ty>::session_mgr() const
    {
        return *session_mgr_ptr;
    }


    template <TLS_policy TLS_Ty>
    typename TLS_services<TLS_Ty>::Policy& 
    TLS_services<TLS_Ty>::policy() const
    {
        return *policy_ptr;
    }   


    template <TLS_policy TLS_Ty>
    typename TLS_services<TLS_Ty>::Random_number_generator& 
    TLS_services<TLS_Ty>::rng() const
    {
        return TLS_services_traits<TLS_Ty>::rng();
    }

} // namesoace Navtech::TLS

#endif // TLS_SERVICES_H