#if 0


#include <iostream>

#include "TLS_services.h"

using namespace Networking;

int main()
{
    TLS_services<TLS_policy::botan> tls_services { };

    auto& session_mgr = tls_services.session_mgr();
    TLS_services<TLS_policy::botan>::Random_number_generator& rand_num { tls_services.rng() };
}

#else


#include <iostream>
#include <algorithm>
#include <iomanip>
#include <thread>
#include <chrono>

#include "Comms_server.h"
#include "Config.h"
#include "Message_generator.h"

using namespace std;
using namespace std::chrono_literals;
using namespace Networking;

int main()
{
    TRACE_MSG("Starting TCP server");
    
    Config config { };
    config.load();

    Comms_server comms_server { config };
    comms_server.start();

    Session::Pointer session { };
    do {
        session = comms_server.get_session(1);
        this_thread::sleep_for(1s);
    } while (session == nullptr);


    session->open();

#ifdef SENDER

    Message_generator message_generator { };

    thread sender_thread {
        [&session, &message_generator]()
        {
            size_t num_messages { 0 };

            while (true) {
            // for (int i { 0 }; i < 1000; ++i) {
                
                if (!session->is_open()) break;
                
                session->send_message(
                    message_generator.random_type(), 
                    message_generator.fixed_string(250'000)
                );

                ++num_messages;
                // if (num_messages % 10 == 0) {
                //     cerr << "Messages sent: " << num_messages << "\r";
                // }
                    
                std::this_thread::sleep_for(100ms);
            }
            cerr << "\n\rSend finished" << endl;
            // session->close();
            while (true);
        }
    };

    sender_thread.join();

#else
    
    thread receiver_thread {
        [&session_mgr, &session]()
        {
            size_t num_messages { 0 };

            while (true) {
                session = session_mgr.get_session_ptr(1);
                if (session) {
                    auto msg = session->get_message();
                    ++num_messages;
                    if (num_messages % 1000 == 0) {
                        cerr << "Messages received: " << num_messages << "\r";
                    }

                    // auto colossus_msg = Networking::Colossus::TCP_message::overlay_onto(msg.data());
                    
                    // cerr << "Signature: ";
                    // cerr << hex;
                    // for_each(
                    //     colossus_msg->signature_begin(), 
                    //     colossus_msg->signature_end(), 
                    //     [](const auto& chr) { cerr << static_cast<unsigned>(chr) << " "; }
                    // );
                    // cerr << dec;
                    // cerr << endl;

                    // cerr << "Type:      " << static_cast<unsigned int>(colossus_msg->type()) << endl;
                    // cerr << "Payload:   " << colossus_msg->payload_size() << endl;
                }
                else {
                    break;
                }
            }
        }
    };

    receiver_thread.join();

#endif // SENDER
    
    TRACE_MSG("Finished");
}

#endif
