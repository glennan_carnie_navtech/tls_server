#include <functional>

#include "Comms_server.h"
#include "Config.h"


namespace Networking {
    
    Comms_server::Comms_server(Config& cfg) :
        session_mgr { io_context, cfg },
        tcp_server  { io_context, cfg.server_config, session_mgr }
    {
    }

    void Comms_server::start()
    {
        using std::thread;
        using std::bind;

        session_mgr.start();
        tcp_server.start();

        t = thread { bind(&Comms_server::run, this) };
        t.detach();
    }


    void Comms_server::run()
    {
        using Work_guard = asio::executor_work_guard<asio::io_context::executor_type>;

        Work_guard work_guard { io_context.get_executor() };
        io_context.run();
    }


    void Comms_server::stop()
    {
        session_mgr.stop();
        tcp_server.stop();
    }


    Session::Pointer Comms_server::get_session(Session::ID id)
    {
        return session_mgr.get_session(id);
    }

} // namespace Networking