#ifndef MESSAGE_H
#define MESSAGE_H

#include <vector>
#include <cstdint>


namespace Networking {

    // A Message is a raw, uninterpreted, container
    // of data. Messages are sent across the network
    // to be interpreted by clients and servers
    //
   using Message = std::vector<std::uint8_t>;
   
} // namespace Networking

#endif // MESSASGE_H