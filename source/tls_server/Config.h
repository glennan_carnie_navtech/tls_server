#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include <cstdint>
#include "Security_config.h"


namespace Networking {

    struct Client_config {
        std::string server_addr;
        std::string port;
    };


    struct Server_config {
        std::uint16_t listen_port;
    };


    struct Config {
        Security::Config client_security {
            "/systemconfig/client.cert",
            "/systemconfig/client.key",
            ""
        };

        Security::Config server_security {
            "/systemconfig/server.cert",
            "/systemconfig/server.key",
            ""
        };

        Client_config client_config {
            "127.0.0.1",
            "56317"
        };

        Server_config server_config {
            56317
        };

        void store() { }
        void load()  { }
    };

} // namespace Networking

#endif // CONFIG_H