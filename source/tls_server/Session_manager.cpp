#include "Session_manager.h"
#include "Security_config.h"
#include "TLS_services.h"
#include "Config.h"

using namespace asio::ip;
using namespace std;

namespace Networking {

    Session_manager::Session_manager(asio::io_context& cntxt, Config& cfg) :
        io_context   { &cntxt },
        config       { &cfg },
        tls_services { cfg }
    {
    }


    void Session_manager::start()
    {
    }
        
    
    void Session_manager::stop()
    {
    }


    void Session_manager::create_session(asio::ip::tcp::socket socket)
    {
        lock_guard lock { mtx };

        auto id = next_id();

        sessions.emplace(
            id, 
            Session::create(
                *io_context,
                move(socket),
                tls_services,
                *this, 
                id
            )
        );
    }


    Session::Pointer Session_manager::get_session(Session::ID id)
    {
        lock_guard lock { mtx };

        if (auto it = sessions.find(id); it != end(sessions)) {
            return it->second.get();
        }
        else {
            return nullptr;
        }
    }



    std::vector<Session::Pointer> Session_manager::all_sessions()
    {
        std::vector<Session::Pointer> results { };

        for (auto& session_pair : sessions) {
            results.push_back(session_pair.second.get());
        }

        return results;
    }
    

    bool Session_manager::remove(Session::ID id)
    {
        lock_guard lock { mtx };

        if (auto it = sessions.find(id); it != end(sessions)) {
            sessions.erase(it);
            return true;
        }
        else {
            return false;
        }
    }


    Session::ID Session_manager::next_id()
    {
        static Session::ID id { 1 };
        return id++;
    }

}