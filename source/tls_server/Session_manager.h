#ifndef SESSION_MANAGER_H
#define SESSION_MANAGER_H

#include <unordered_map>
#include <vector>
#include <mutex>

#include "asio.hpp"

#include "Session.h"
#include "Security_config.h"
#include "TLS_services.h"

#include "Server_credentials_manager.h"
#include "Policy.h"


namespace Networking {

    class Config;

    class Session_manager {
    public:
        Session_manager(asio::io_context& cntxt, Config& cfg);

        void start();
        void stop();

        void create_session(asio::ip::tcp::socket socket);
        Session::Pointer get_session(Session::ID id);
        std::vector<Session::Pointer> all_sessions();
        bool remove(Session::ID id);
        
    protected:
        friend class Session;
        
        

    private:
        asio::io_context* io_context;
        Config* config;

        // Session management
        //
        std::mutex mtx { };
        std::unordered_map<Session::ID, Session::Owning_ptr> sessions { };
        Session::ID next_id();
        
        // Security (TLS) components
        // Instantiated on creation of first session
        //
        TLS_services<TLS_policy::botan> tls_services;
    };

} // namespace Networking

#endif // SESSION_MANAGER_H