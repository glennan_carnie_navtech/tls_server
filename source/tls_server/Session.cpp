#include <iostream>

#include "Session.h"
#include "Session_manager.h"
#include "TLS_services.h"

using namespace asio::ip;
using namespace std;


namespace Networking {

    Session::Owning_ptr Session::create(
        asio::io_context&            context,
        asio::ip::tcp::socket        s,
        TLS_services<TLS_policy::botan>& tls_services,
        Session_manager&             parent, 
        Session::ID                  identity
    )
    {
        return Owning_ptr {
            new Session {
                context,
                move(s),
                tls_services,
                parent,
                identity
            }
        };
    }

    Session::Session(
        asio::io_context&            context,
        asio::ip::tcp::socket        s,
        TLS_services<TLS_policy::botan>& tls_services,
        Session_manager&             parent, 
        Session::ID                  identity
    ) :
        io_context  { &context },
        connection  { context, move(s), *this },
        tls_engine  { tls_services, *this },
        translator  { *this },
        session_mgr { &parent },
        id          { identity }
    {
        // Form the comms pipeline
        //
        connection.rx.forward_to(tls_engine.decrypt_in);
        tls_engine.decrypt_out.forward_to(translator.parse_in);

        translator.marshall_out.forward_to(tls_engine.encrypt_in);
        tls_engine.encrypt_out.forward_to(connection.tx);

        // Connections for non-TLS server
        //
        // connection.rx.forward_to(translator.parse_in);
        // translator.marshall_out.forward_to(connection.tx);
    }


    Session::Flags::Bitmask Session::all_component_flags() const
    {
        return static_cast<Session::Flags::Bitmask>(
            (1 << (static_cast<unsigned>(Session::Component::connection))) |
            (1 << (static_cast<unsigned>(Session::Component::tls_engine))) |
            (1 << (static_cast<unsigned>(Session::Component::translator)))
        );
    }


    bool Session::is_open() const
    {
        return active_flags.try_wait_all(all_component_flags());
    }


    void Session::open()
    {
        TRACE_MSG("Starting session");
        connection.open();
        tls_engine.open();
        translator.open();

        // Block until all components signal active
        //
        active_flags.wait_all(all_component_flags());
    }
    
    
    void Session::close()
    {
        TRACE_MSG("Closing session");
        send_queue.clear();
        receive_queue.clear();
        
        translator.close();
        tls_engine.close();
        connection.close();

        session_mgr->remove(id);
    }


    void Session::send_message(Colossus::TCP_message::Type type, std::string data)
    {
        using std::move;

        // Post for asynchronous sending
        // 
        send_queue.push(Session::Message_pair { type, move(data) });
        asio::post(*io_context, bind(&Session::do_send, this));
    }


    void Session::report_internal_failure(Session::Component component [[maybe_unused]])
    {
        close();
    }


    void Session::do_send()
    {
        using std::move;

        if (is_open()) {
            if (!send_queue.empty()) {
                auto msg_pair = send_queue.try_pop();
                if (msg_pair.has_value()) {
                    translator.marshall_in.post(move(msg_pair.value()));
                }
            }
        }
    }


    Message Session::get_message()
    {
        return receive_queue.pop();
    }


    void Session::active(Session::Component component)
    {
        active_flags[static_cast<int>(component)] = 1;
    }


    void Session::inactive(Session::Component component)
    {
        active_flags[static_cast<int>(component)] = 0;
    }


} // namespace Networking