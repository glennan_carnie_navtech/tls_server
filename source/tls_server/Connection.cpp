#include <iostream>
#include <cassert>
#include <cstring>

#include "Connection.h"
#include "Session.h"
#include "Memory_types.h"

#include "Colossus_message.h"

using namespace asio::ip;
using namespace std;
using namespace Utility::Memory_literals;


namespace Networking {

    // ------------------------------------------------------------------------
    // Input channel
    //
    Connection::Input_channel::Input_channel(asio::io_context& context, asio::ip::tcp::socket& s, Connection& parent) :
        rx_strand  { context },
        socket     { &s },
        connection { &parent },
        timeout    { context }
    {
        timeout.on_expiry([this] { connection->rx_error(asio::error::timed_out); });
    }

    
    void Connection::Input_channel::open()
    {
        do_read();
    }
    
    
    void Connection::Input_channel::close()
    {
    }


    void Connection::Input_channel::do_read()
    {
        if (socket->is_open()) {
            socket->async_read_some(
                asio::buffer(rx_buffer.data(), rx_buffer.size()),
                rx_strand.wrap(
                    bind(
                        &Connection::Input_channel::on_read_complete, 
                        this, 
                        placeholders::_1, 
                        placeholders::_2
                    )
                )
            );
            timeout.expire_after(rx_timeout_duration);
        }
    }
    
    
    void Connection::Input_channel::on_read_complete(const asio::error_code& err, std::size_t num_bytes)
    {
        timeout.cancel();
        
        if (!err) {
            bytes_recvd += num_bytes;
            // 
            cerr << "Bytes received: " << bytes_recvd << "\n\r";
        
            Message msg { begin(rx_buffer), begin(rx_buffer) + num_bytes };
            connection->rx.post(move(msg));
        }
        else {
            connection->rx_error(err);
        }
        do_read();
    }


    // ------------------------------------------------------------------------
    // Output channel
    //
    Connection::Output_channel::Output_channel(asio::io_context& context, asio::ip::tcp::socket& s, Connection& parent) :
        tx_strand  { context },
        socket     { &s },
        connection { &parent },
        timeout    { context }
    {
        // 16k is the maximum size of a TLS record
        //
        send_buffer.reserve(256_kB);
        timeout.on_expiry([this] { connection->tx_error(asio::error::timed_out); });
    }


    void Connection::Output_channel::open()
    {
       //  do_write();
    }


    void Connection::Output_channel::close()
    {
        send_buffer.clear();
    }


    void Connection::Output_channel::send(const Message& msg)
    {
        using std::begin;
        using std::end;
        
        pending_buffer.insert(
            end(pending_buffer),
            begin(msg),
            end(msg)
        );
        do_write();
    }


    void Connection::Output_channel::send(Message&& msg)
    {
        using std::begin;
        using std::end;
    
        pending_buffer.insert(
            end(pending_buffer),
            begin(msg),
            end(msg)
        );
        do_write();
    }


    void Connection::Output_channel::do_write()
    {
        using std::swap;

        if (socket->is_open()) {

            if (send_buffer.empty() && !pending_buffer.empty()) {

                swap(pending_buffer, send_buffer);

                asio::async_write(
                    *socket,
                    asio::buffer(send_buffer),
                    tx_strand.wrap(
                        bind(
                            &Connection::Output_channel::on_write_complete, 
                            this, 
                            placeholders::_1, 
                            placeholders::_2
                        )
                    )
                );

                timeout.expire_after(tx_timeout_duration);
            }
        }
    }


    // void Connection::Output_channel::do_write()
    // {
    //     using std::swap;

    //     if (socket->is_open()) {
        
    //         if (send_buffer.empty() && !pending_buffer.empty()) {

    //             swap(pending_buffer, send_buffer);

    //             asio::error_code err { };

    //             socket->write_some(
    //                 asio::buffer(pending_buffer),
    //                 err
    //             );

    //             assert(!err);
    //         }
    //     }
    // }


    void Connection::Output_channel::on_write_complete(const asio::error_code& err, size_t num_bytes)
    {
        timeout.cancel();

        if (err) {
            if (err != asio::error::operation_aborted) {
                connection->tx_error(err);
            }
        }

        bytes_sent += num_bytes;
        cerr << "Bytes sent: " << bytes_sent << "\r";
        send_buffer.clear();

        do_write();
    }


    // ------------------------------------------------------------------------
    // Connection
    //
    Connection::Connection(asio::io_context& io, asio::ip::tcp::socket s, Session& parent) :
        socket   { move(s) },
        receiver { io, socket, *this },
        sender   { io, socket, *this },
        session  { &parent }
    {
        tx.on_receive([this](Message& msg){ sender.send(move(msg)); });

        // Set the socket tx/rx buffer size
        //
        // asio::socket_base::send_buffer_size send_buf_sz(1_Mbyte);
        // socket.set_option(send_buf_sz);

        // asio::socket_base::receive_buffer_size recv_buf_sz(1024_kbyte);
        // socket.set_option(recv_buf_sz);
        
    }


    void Connection::open()
    {
        receiver.open();
        sender.open();
        session->active(Session::Component::connection);
    }
    
    
    void Connection::close()
    {
        session->inactive(Session::Component::connection);
        socket.close();
        sender.close();
        receiver.close();
    }


    void Connection::rx_error(const asio::error_code& err)
    {
        TRACE_MSG(err.message());
        session->report_internal_failure(Session::Component::connection);
    }


    void Connection::tx_error(const asio::error_code& err)
    {
        TRACE_MSG(err.message());
        session->report_internal_failure(Session::Component::connection);
    }
    

} // namespace Networking