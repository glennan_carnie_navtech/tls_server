#ifndef TLS_POLICY_H
#define TLS_POLICY_H

namespace Networking {

    enum class TLS_policy { botan, other };

}

#endif // TLS_POLICY_H