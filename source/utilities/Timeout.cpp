#include <cassert>
#include "Timeout.h"
#include "trace.h"

using namespace std;
using asio::io_context;
using asio::steady_timer;
using asio::error_code;


namespace Utility {

    Timeout::Timeout(asio::io_context& context) :
        timer { context }
    {
    }
    

    void Timeout::expire_after(const Duration& timeout)
    {
        if (timeout == Timeout::forever) return;
        
        timer.expires_from_now(timeout);
        timer.async_wait(bind(&Timeout::on_timer_expired, this, std::placeholders::_1));
    }


    void Timeout::cancel()
    {
        timer.cancel();
    }



    void Timeout::on_timer_expired(const asio::error_code& error)
    {
        // Timeout has been cancelled, rather than
        // timing-out.
        //
        if (error == asio::error::operation_aborted) return;
            
        if (timeout_callback != nullptr) {
            timeout_callback();
        }
    }


    void Timeout::on_expiry(std::function<void()> fn)
    {
        timeout_callback = move(fn);
    }

} // namespace Utility