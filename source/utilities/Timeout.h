#ifndef TIMEOUT_H
#define TIMEOUT_H

#include <functional>
#include <chrono>
#include "asio.hpp"

namespace Utility {

    class Timeout {
    public:
        using Duration = std::chrono::seconds;
        static constexpr Duration forever { Duration::max() };

        Timeout(asio::io_context& context);

        void expire_after(const Duration& timeout);
        void cancel();
        void on_expiry(std::function<void()> fn);

    protected:
        void on_timer_expired(const asio::error_code& error);

    private:
        asio::steady_timer    timer;
        std::function<void()> timeout_callback { };
    };
    
} // namespace Utility

#endif // TIMEOUT_H