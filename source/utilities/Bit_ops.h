#ifndef BIT_OPS_H
#define BIT_OPS_H

#include <cstdint>
#include <utility>

namespace Utility {

    // Creates a std::uint32_t where bit n is set
    //
    inline constexpr
    std::uint32_t bit(std::uint32_t n)
    {
        if (n > ((sizeof(uint32_t) * 8) - 1)) return 0;  // Overflow
        return (1 << n);
    }


    // Creates a std::uint32_t where bits start -> end
    // (inclusive) are set
    //
    inline constexpr
    std::uint32_t bit_range(std::uint32_t start, std::uint32_t end)
    {
        std::uint32_t value { };

        if (start > end) std::swap(start, end);
        if (end > ((sizeof(uint32_t) * 8) - 1)) end = ((sizeof(uint32_t) * 8) - 1);

        for (auto i = start; i <= end; ++i) {
            value |= bit(i);
        }
        return value;
    }


    // Returns true if bit b is set in word
    //
    inline constexpr
    bool is_set(std::uint32_t word, std::uint32_t b)
    {
        return ((word & bit(b)) != 0);
    }

} // namespace Utility



#endif // BIT_OPS_H