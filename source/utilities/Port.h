#ifndef PORT_H
#define PORT_H


#include <functional>
#include <string>


namespace Utility {

    template <typename T>
    class Port {
    public:
        using Callback = std::function<void(T&)>;
        using Value    = T;

        void forward_to(Port& dst)
        {
            other = &dst;
        }

        template <typename U>
        void post(U&& in)
        {
            // Forward to connected port
            //
            if (other) {
                other->value = std::forward<U>(in);
                if (other->recv_cb) {
                    other->recv_cb(other->value);
                }
            }
            // Hold locally
            //
            else {                            
                value = std::forward<U>(in);
                if (recv_cb) {
                    recv_cb(value);
                }
            }
        }

        template <typename U>
        void operator<<(U&& in)
        {
            post(std::forward<U>(in));
        }
        

        void on_receive(Callback fn)
        {
            recv_cb = std::move(fn);
        }

    private:
        T        value   { };
        Port*    other   { };
        Callback recv_cb { };
    };

} // namespace Utility


#endif // PORT_H