#ifndef FIFO_H
#define FIFO_H

#include <queue>
#include <optional>

namespace Utility {

    template <typename T>
    class FIFO : private std::queue<T> {
    public:
        template <typename U> void push(U&& in_val);
        T pop();
        std::optional<T> try_pop();
        void clear();
        
        using std::queue<T>::empty;
        using std::queue<T>::size;
    
    private:
        using Queue = std::queue<T>;
    };


    template <typename T>
    template <typename U> 
    void FIFO<T>::push(U&& in_val)
    {
        Queue::push(std::forward<U>(in_val));
    }


    template <typename T>
    T FIFO<T>::pop()
    {
        if (Queue::empty()) throw std::runtime_error { "Empty FIFO" };
        
        T out_val { Queue::front() };
        Queue::pop();
        return out_val;
    }


    template <typename T>
    std::optional<T> FIFO<T>::try_pop()
    {
        if (empty()) return std::nullopt;

        std::optional<T> out_val { move(Queue::front()) };
        Queue::pop();
        return out_val;
    }


    template <typename T>
    void FIFO<T>::clear()
    {
        while (!Queue::empty()) {
            Queue::pop();
        }
    }


}

#endif // FIFO_H