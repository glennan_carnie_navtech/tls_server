#ifndef THREADSAFE_QUEUE_H
#define THREADSAFE_QUEUE_H


#include <queue>
#include <optional>
#include <mutex>
#include <condition_variable>
#include "trace.h"

namespace Utility {
    
    template <typename T>
    class alignas(alignof(std::condition_variable)) Threadsafe_queue : private std::queue<T> {
    public:
        template <typename U> void push(U&& in_val);
        T pop();
        std::optional<T> try_pop();
        void clear();
        bool empty() const;
        std::size_t size() const;

    private:
        using Queue = std::queue<T>;

        mutable std::mutex mtx { };
        std::condition_variable has_data { };
    };


    template <typename T>
    template <typename U> 
    void Threadsafe_queue<T>::push(U&& in_val)
    {
        std::lock_guard lock { mtx };

        Queue::push(std::forward<U>(in_val));
        has_data.notify_all();
    }


    template <typename T>
    T Threadsafe_queue<T>::pop()
    {
        std::unique_lock lock { mtx };
        
        // while (Queue::empty()) {
        //     has_data.wait(lock);
        // }
        
        has_data.wait(lock, [this] { return !Queue::empty(); });

        T out_val { Queue::front() };
        Queue::pop();
        return out_val;
    }


    template <typename T>
    std::optional<T> Threadsafe_queue<T>::try_pop()
    {
        std::unique_lock lock { mtx };

        if (Queue::empty()) return std::nullopt;

        std::optional<T> out_val { move(Queue::front()) };
        Queue::pop();
        return out_val;
    }


    template <typename T>
    bool Threadsafe_queue<T>::empty() const
    {
        std::lock_guard lock { mtx };

        return Queue::empty();
    }


    template <typename T>
    std::size_t Threadsafe_queue<T>::size() const
    {
        std::lock_guard lock { mtx };
    
        return Queue::size();
    }


    template <typename T>
    void Threadsafe_queue<T>::clear()
    {
        std::lock_guard lock { mtx };
    
        while (!Queue::empty()) {
            Queue::pop();
        }
    }

} // namespace Utility

#endif // THREADSAFE_QUEUE_H 